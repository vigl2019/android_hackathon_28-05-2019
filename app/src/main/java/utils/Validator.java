package utils;

import android.content.Context;
import android.content.res.Resources;

import com.example.finance.R;

public class Validator {

    // Check text
    public static boolean checkTextView(String text) {
        if (text.trim().isEmpty())
            return false;

        if (Integer.parseInt(text) == 0)
            return false;

        return true;
    }

    // Check month deposit count >= 6 and <= 36
    public static String checkMonthDepositCount(String monthDepositCount, Context context) {

        Resources resources = context.getResources();

        if(!checkTextView(monthDepositCount))
            return resources.getText(R.string.minimum_month_deposit_count_error).toString();

        String minimumMonthDepositCount = Helper.getPropertyValue(context.getString(R.string.minimum_month_deposit_count), context);
        String maximumMonthDepositCount = Helper.getPropertyValue(context.getString(R.string.maximum_month_deposit_count), context);

        if (Integer.parseInt(monthDepositCount.trim()) < Integer.parseInt(minimumMonthDepositCount))
            return resources.getText(R.string.minimum_month_deposit_count_error).toString();

        if (Integer.parseInt(monthDepositCount.trim()) > Integer.parseInt(maximumMonthDepositCount))
            return resources.getText(R.string.maximum_month_deposit_count_error).toString();

        return "";
    }

    public static String checkAnnualPercentRate(String annualPercentRate, Context context) {

        Resources resources = context.getResources();

        if(!checkTextView(annualPercentRate))
            return resources.getText(R.string.minimum_annual_percent_rate_error).toString();

        String minimumAnnualPercentRate = Helper.getPropertyValue(context.getString(R.string.minimum_annual_percent_rate), context);
        String maximumAnnualPercentRate = Helper.getPropertyValue(context.getString(R.string.maximum_annual_percent_rate), context);

        if (Float.parseFloat(annualPercentRate.trim()) < Float.parseFloat(minimumAnnualPercentRate))
            return resources.getText(R.string.minimum_annual_percent_rate_error).toString();

        if (Float.parseFloat(annualPercentRate.trim()) > Float.parseFloat(maximumAnnualPercentRate))
            return resources.getText(R.string.maximum_annual_percent_rate_error).toString();

        return "";
    }

    public static String checkPercentIncomeTax(String percentIncomeTax, Context context) {

        Resources resources = context.getResources();

        if(!checkTextView(percentIncomeTax))
            return resources.getText(R.string.minimum_percent_income_tax_error).toString();

        String minimumPercentIncomeTax = Helper.getPropertyValue(context.getString(R.string.minimum_percent_income_tax), context);
        String maximumPercentIncomeTax = Helper.getPropertyValue(context.getString(R.string.maximum_percent_income_tax), context);

        if (Float.parseFloat(percentIncomeTax.trim()) < Float.parseFloat(minimumPercentIncomeTax))
            return resources.getText(R.string.minimum_percent_income_tax_error).toString();

        if (Float.parseFloat(percentIncomeTax.trim()) > Float.parseFloat(maximumPercentIncomeTax))
            return resources.getText(R.string.maximum_percent_income_tax_error).toString();

        return "";
    }
}
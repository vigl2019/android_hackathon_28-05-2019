package utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.widget.Toast;

import com.example.finance.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Properties;

public class Helper {

    public static float convertIntegerToFloat(Integer value) {
        return Float.parseFloat(value.toString());
    }

    public static double convertIntegerToDouble(Integer value) {
        return Double.parseDouble(value.toString());
    }

    public static double convertFloatToDouble(Float value) {

/*
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat decimalFormat = new DecimalFormat("#.##", symbols);

        DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        value = Float.parseFloat(decimalFormat.format(value));
*/

        value = Float.parseFloat(convertNumberToString(value));
        return Double.parseDouble(value.toString());
    }

    public static float convertDoubleToFloat(Double value) {

/*
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        value = Double.valueOf(decimalFormat.format(value));
*/

        value = Double.parseDouble(convertNumberToString(value));
        return Float.parseFloat(value.toString());
    }

    public static double roundUpToTwoDecimalPlaces(float value) {

/*
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        value = Float.valueOf(decimalFormat.format(value));
*/
        return Double.parseDouble(convertNumberToString(value));
    }

    public static String convertNumberToString(Number value){
        Locale locale = Locale.ENGLISH;
//      NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
//      NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(2); // round to 2 digits

        return numberFormat.format(value);
    }

    //================================================================================//

    public static String getPropertyValue(String propertyName, Context context){

        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream inputStream = assetManager.open(context.getString(R.string.properties_file_name));
            properties.load(inputStream);
        }
        catch (IOException e){
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return properties.getProperty(propertyName);
    }
}
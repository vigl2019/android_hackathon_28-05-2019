package model;

public final class Deposit {

    // Налог на прибыль (% от суммы начисленных процентов по вкладу)

    /*

incomeTaxSum - сумма налога на прибыль

inputDepositSum - сумма, которую принес клиент в начале срока депозита
percentDepositSum - сумма начисленных процентов по вкладу
percentIncomeTax - процент налога на прибыль = значение из поля "Налог в %"

     */

    public static double getIncomeTaxSum(float inputDepositSum, int monthDepositCount, float percentIncomeTax,
                                         float monthlyAdditionalDepositSum, float annualInterestRate, boolean capitalization) {

        double incomeTaxSum = 0;

        float fourthNumber = inputDepositSum + monthlyAdditionalDepositSum;

        if (capitalization) {
            double firstNumber = 1 + (annualInterestRate / 100 / 12);
            double secondNumber = Double.parseDouble(String.valueOf(monthDepositCount));
            double thirdNumber = Math.pow(firstNumber, secondNumber);

            double percentDepositSum = (fourthNumber * thirdNumber) - inputDepositSum;
            incomeTaxSum = percentDepositSum * (percentIncomeTax / 100);
        } else {
            float percentDepositSum = fourthNumber * (annualInterestRate / 100 / 12) * monthDepositCount;
            incomeTaxSum = percentDepositSum * (percentIncomeTax / 100);
        }

//      return Float.parseFloat(String.valueOf(incomeTaxSum));
        return incomeTaxSum;
    }

    //================================================================================//

    // Ежемесячный доход - доход по депозиту за вычетом налога и без учета капитализации и пополнений

    /*

monthlyIncome - ежемесячный доход

inputDepositSum - сумма, которую принес клиент в начале срока депозита
monthlyIncomeTax - налог на прибыль за месяц
percentIncomeTax - процент налога на прибыль = значение из поля "Налог в %"
annualInterestRate - годовая процентная ставка = значение из поля "Ставка %"

     */

    public static float getMonthlyIncomeSum(float inputDepositSum, float percentIncomeTax, float annualInterestRate) {

        float monthlyIncomeWithOutTax = inputDepositSum * (annualInterestRate / 100 / 12);
        float monthlyIncomeTax = monthlyIncomeWithOutTax * (percentIncomeTax / 100);
        float monthlyIncome = monthlyIncomeWithOutTax - monthlyIncomeTax;

        return monthlyIncome;
    }

    //================================================================================//

// Общий доход - итоговый чистый доход за весь период
// (Итоговая сумма для получения <<минус>> Сумма, которую принес клиент в начале срока депозита)

/*

totalNetIncome - Общий доход - итоговый чистый доход за весь период

inputDepositSum - сумма, которую принес клиент в начале срока депозита
percentDepositSum - сумма начисленных процентов по вкладу
totalAdditionalDepositSum - общая сумма пополнений
percentIncomeTax - процент налога на прибыль = значение из поля "Налог в %"
incomeTax - сумма налога на прибыль
annualInterestRate - годовая процентная ставка = значение из поля "Ставка %"

 */

    public static double getTotalNetIncome(float inputDepositSum, int monthDepositCount, float percentIncomeTax,
                                           float monthlyAdditionalDepositSum, float annualInterestRate, boolean capitalization) {

        double returnDepositSum = getReturnDepositSum(inputDepositSum, monthDepositCount, percentIncomeTax,
                monthlyAdditionalDepositSum, annualInterestRate, capitalization);

        return returnDepositSum - inputDepositSum;
    }

    //================================================================================//

    // Общая сумма пополнений - сумма всех пополнений за весь период депозита

/*

totalAdditionalDepositSum - общая сумма пополнений

monthlyAdditionalDepositSum - ежемесячная сумма пополнений (значение из поля "Пополнение (мес)")
monthDepositCount - количество месяцев депозита (значение из поля "Срок (мес)")

 */

    public static float getTotalAdditionalDepositSum(float monthlyAdditionalDepositSum, int monthDepositCount) {

        float totalAdditionalDepositSum = monthlyAdditionalDepositSum * monthDepositCount;
        return totalAdditionalDepositSum;
    }

    //================================================================================//

    // Итоговая сумма для получения - (тело депозита + все пополнения + все начисленные проценты - налоги)

/*

returnDepositSum - Итоговая сумма для получения (сумма, которую получит клиент после окончания срока депозита)

inputDepositSum - сумма, которую принес клиент в начале срока депозита
inputDepositSum - сумма, которую принес клиент в начале срока депозита
totalAdditionalDepositSum - общая сумма пополнений
percentDepositSum - сумма начисленных процентов по вкладу
percentIncomeTax - процент налога на прибыль = значение из поля "Налог в %"
incomeTax - сумма налога на прибыль
monthDepositCount - количество месяцев депозита (значение взять из поля "Срок (мес)")
annualInterestRate - значение из поля "Ставка %";

*/

    public static double getReturnDepositSum(float inputDepositSum, int monthDepositCount, float percentIncomeTax,
                                             float monthlyAdditionalDepositSum, float annualInterestRate, boolean capitalization) {

        double incomeTaxSum = getIncomeTaxSum(inputDepositSum, monthDepositCount, percentIncomeTax,
                monthlyAdditionalDepositSum, annualInterestRate, capitalization);

        double returnDepositSum = 0;
        float fourthNumber = inputDepositSum + monthlyAdditionalDepositSum;

        if (capitalization) {
            double firstNumber = 1 + (annualInterestRate / 100 / 12);
            double secondNumber = Double.parseDouble(String.valueOf(monthDepositCount));
            double thirdNumber = Math.pow(firstNumber, secondNumber);

            returnDepositSum = (fourthNumber * thirdNumber) - incomeTaxSum;


        } else {
            returnDepositSum = inputDepositSum +
                               (fourthNumber * monthDepositCount * (annualInterestRate / 100 / 12)) - incomeTaxSum;
        }
        return returnDepositSum;
    }
}

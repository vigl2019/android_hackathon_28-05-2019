package com.example.finance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import model.Deposit;
import utils.Helper;
import utils.Validator;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.am_input_deposit_sum)
    EditText etInputDepositSum;

    @BindView(R.id.am_annual_percent_rate)
    EditText etAnnualPercentRate;

    @BindView(R.id.am_month_deposit_count)
    EditText etMonthDepositCount;

    @BindView(R.id.am_capitalization)
    CheckBox cbCapitalization;

    @BindView(R.id.am_monthly_additional_deposit_sum)
    EditText etMonthlyAdditionalDepositSum;

    @BindView(R.id.am_percent_income_tax)
    EditText etPercentIncomeTax;

    @BindView(R.id.am_monthly_income)
    TextView tvMonthlyIncome;

    @BindView(R.id.am_total_net_income)
    TextView tvTotalNetIncome;

    @BindView(R.id.am_total_additional_deposit_sum)
    TextView tvTotalAdditionalDepositSum;

    @BindView(R.id.am_return_deposit_sum)
    TextView tvReturnDepositSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        etInputDepositSum.requestFocus();


        cbCapitalization.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (checkTextViews()) {
                    calculateDeposit();
                }
            }
        });
    }

    //================================================================================//

    @OnTextChanged({R.id.am_input_deposit_sum, R.id.am_month_deposit_count, R.id.am_monthly_additional_deposit_sum,
                    R.id.am_annual_percent_rate, R.id.am_percent_income_tax})
    public void onTextChanged(CharSequence value) {

        if (checkTextViews()) {

            if (checkTextCorrectValues()) {
                calculateDeposit();
            } else {
                clearOutputData();
            }
        } else {
            clearOutputData();
        }
    }

    //================================================================================//

    private void calculateDeposit() {

        float inputDepositSum = Float.parseFloat(etInputDepositSum.getText().toString().trim());
        int monthDepositCount = Integer.parseInt(etMonthDepositCount.getText().toString().trim());

        float monthlyAdditionalDepositSum = 0;
        if (!etMonthlyAdditionalDepositSum.getText().toString().trim().isEmpty())
            monthlyAdditionalDepositSum = Float.parseFloat(etMonthlyAdditionalDepositSum.getText().toString().trim());

        float annualInterestRate = Float.parseFloat(etAnnualPercentRate.getText().toString().trim());
        float percentIncomeTax = Float.parseFloat(etPercentIncomeTax.getText().toString().trim());
        boolean capitalization = cbCapitalization.isChecked();

        // Ежемесячный доход - доход по депозиту за вычетом налога и без учета капитализации и пополнений
        float monthlyIncomeSum = Deposit.getMonthlyIncomeSum(inputDepositSum, percentIncomeTax, annualInterestRate);

        // Общий доход - итоговый чистый доход за весь период
        double totalNetIncome = Deposit.getTotalNetIncome(inputDepositSum, monthDepositCount, percentIncomeTax,
                monthlyAdditionalDepositSum, annualInterestRate, capitalization);

        // Общая сумма пополнений - сумма всех пополнений за весь период депозита
        float totalAdditionalDepositSum = Deposit.getTotalAdditionalDepositSum(monthlyAdditionalDepositSum, monthDepositCount);

        // Итоговая сумма для получения - (тело депозита + все пополнения + все начисленные проценты - налоги)
        double returnDepositSum = Deposit.getReturnDepositSum(inputDepositSum, monthDepositCount, percentIncomeTax,
                monthlyAdditionalDepositSum, annualInterestRate, capitalization);

        tvMonthlyIncome.setText("Ежемесячный доход: " + Helper.convertNumberToString(monthlyIncomeSum));
        tvTotalNetIncome.setText("Общий доход через " + monthDepositCount + " мес.: " + Helper.convertNumberToString(totalNetIncome));
        tvTotalAdditionalDepositSum.setText("Общая сумма пополнений: " + Helper.convertNumberToString(totalAdditionalDepositSum));
        tvReturnDepositSum.setText("Итоговая сумма (% + вклад): " + Helper.convertNumberToString(returnDepositSum));
    }

    //================================================================================//

    private boolean checkTextViews() {
        if (!Validator.checkTextView(etInputDepositSum.getText().toString().trim())) {
            return false;
        }

        if (!Validator.checkTextView(etMonthDepositCount.getText().toString().trim())) {
            return false;
        }

        if (!Validator.checkTextView(etAnnualPercentRate.getText().toString().trim())) {
            return false;
        }

        if (!Validator.checkTextView(etPercentIncomeTax.getText().toString().trim())) {
            return false;
        }

        return true;
    }

    //================================================================================//

    private boolean checkTextCorrectValues() {

        String errorMessage;

        errorMessage = Validator.checkMonthDepositCount(etMonthDepositCount.getText().toString().trim(), getApplicationContext());

        if (!errorMessage.isEmpty()) {
            createAlertDialog(errorMessage, etMonthDepositCount);
            return false;
        }

        errorMessage = Validator.checkAnnualPercentRate(etAnnualPercentRate.getText().toString().trim(), getApplicationContext());

        if (!errorMessage.isEmpty()) {
            createAlertDialog(errorMessage, etAnnualPercentRate);
            return false;
        }

        errorMessage = Validator.checkPercentIncomeTax(etPercentIncomeTax.getText().toString().trim(), getApplicationContext());

        if (!errorMessage.isEmpty()) {
            createAlertDialog(errorMessage, etPercentIncomeTax);
            return false;
        }

        return true;
    }

    //================================================================================//

    private void createAlertDialog(String errorMassage, final View view) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        dialogBuilder.setTitle("Ошибка вводимых данных!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        view.requestFocus();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    //================================================================================//

    private void clearOutputData(){
        tvMonthlyIncome.setText("");
        tvTotalNetIncome.setText("");
        tvTotalAdditionalDepositSum.setText("");
        tvReturnDepositSum.setText("");
    }
}